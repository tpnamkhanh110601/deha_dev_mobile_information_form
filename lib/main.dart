import 'dart:io';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return   MaterialApp(
      home: HomePage(),
      debugShowCheckedModeBanner: false ,
    );
  }
}

class HomePage extends StatelessWidget{
  static const IconData star = IconData(0xe5f9, fontFamily: 'MaterialIcons');
  static const IconData phone = IconData(0xe4a2, fontFamily: 'MaterialIcons');
  static const IconData telegram = IconData(0xf0586, fontFamily: 'MaterialIcons');
  static const IconData share = IconData(0xe593, fontFamily: 'MaterialIcons');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Information')),

      body: Container(
           child: Column(
             children: [
               Container(
                 child: Row(
                   children:[
                     Expanded(
                     child:Image.asset(
                       ('assets/images/nature.jpeg'),
                       fit: BoxFit.contain,
                     )
                 ),]
                 ),
               ),

               Container(
                 margin: const EdgeInsets.fromLTRB(20,20,20,10),
                 child: Row(
                   children: [
                     Expanded(
                       flex: 1,
                         child: Column(
                           children: [
                             Row(
                               children: const [
                                 Text(
                                   'Oeschinnen Lake Campground',
                                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                                 ),
                               ],
                             ),
                             Row(
                               children: const [
                                 Text(
                                   'Kanderseg, England',
                                   style: TextStyle(color: Colors.grey),
                                   textAlign: TextAlign.left,
                                 ),
                               ],
                             ),
                           ],
                         )
                     ),
                     Column(
                       children: [
                         Row(
                           children: const [
                             Icon(
                               star,
                               color: Colors.red,
                             ),
                             Text('41'),
                           ],
                         ),
                       ],
                     ),
                   ],
                 ),
               ),
               Container(
                 margin: const EdgeInsets.fromLTRB(20,20,20,10),
                 child: Row(
                   children: [
                     Button(phone, 'CALL'),
                     Button(telegram, 'ROUTE'),
                     Button(share, 'SHARE'),
                   ],
                 ),
               ),

               Container(
                 margin: const EdgeInsets.fromLTRB(20,20,20,0),
                 child:  Row(
                   children: const [
                     Expanded(
                       child: Text('In this tutorial, we learned how to pass data to stateful widget '
                           'in Flutter with practical examples. We first saw the problem you may face while '
                           'trying to access the variable without using the stateful widget .A command-line tool which '
                           'simplifies the task of updating your Flutter apps launcher icon. Fully flexible',
                         style: TextStyle(fontSize: 17),
                         textAlign: TextAlign.justify,
                       ),
                     ),
                   ],
                 ),
               )
             ],
           ),
      ),
    );
  }

  Widget Button(var icon, var text){
    return Expanded(
      flex: 1,
      child: Column(
        children: [
          Icon(
            icon,
            color: Colors.blue,
          ),
          Text(
            text,
            style: const TextStyle(color: Colors.blue),
          ),
        ],
      ),
    );
  }
}